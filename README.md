# JsonToDo app

The purpose of this small project is to demonstrate the creation of a MEAN-stack (MongoDB, Express, Angular and Node) application. It was coded as part of an introductory course on Angular taught in Spanish for [elev8](https://www.elev8me.com).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

## Cloning this repo

Run `npm install` to clone this repository in your local machine

## Backend

To start the json-server backend, run `npm run backend`

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Additional Angular commands

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
