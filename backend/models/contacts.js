const mongoose = require('mongoose');

// define Schema
const ContactSchema = mongoose.Schema({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String }
})

const Contact = mongoose.model('Contact', ContactSchema);
module.exports = Contact;