const express = require("express");
const router = express.Router();
const ObjectId = require("mongoose").Types.ObjectId;

const Contact = require("../models/contacts");

// custom routes

// create a contact
// HTTP POST method
router.post("/contact", (req, res) => {
  let new_contact = new Contact({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
  });
  new_contact.save((error, contact) => {
    if (error) {
      console.error(`HTTP POST error: ${error}`);
      res.json({ msg: "Failed to add contact: ", new_contact });
    } else {
      res.json({ msg: "Contact added successfully", contact });
    }
  });
});

// read all contacts
// HTTP GET method
router.get("/contacts", (req, res) => {
  Contact.find((error, contacts) => {
    if (error) {
      console.error(`HTTP GET error: ${error}`);
      res.json({ msg: "Failed to retrieve contacts" });
    }
    res.json(contacts);
  });
});

// read a contact by id
// HTTP GET method
router.get("/contact/:id", (req, res) => {
  let contact_id = req.params.id;
  if (!ObjectId.isValid(contact_id))
    return res
      .status(400)
      .send(`No contact found with given id: ${contact_id}`);
  Contact.findOne({ _id: contact_id }, (error, contact) => {
    if (error) {
      console.error(`HTTP GET error: ${error}`);
      res.json({ msg: "Failed to retrieve contact" });
    } else {
      res.json(contact);
    }
  });
});

// update a contact
// HTTP PUT method
router.put("/contact/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send(`No contact found with given id: ${req.params.id}`);
  const updated_contact = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
  };
  Contact.findByIdAndUpdate(
    req.params.id,
    { $set: updated_contact },
    { new: true }, // option for returning updated document bellow in res.send(document)
    (error, document) => {
      if (!error) {
        res.send(document);
      } else {
        console.error(`Error updating Contact: ${JSON.stringify(error)} `);
      }
    }
  );
});

// delete a contact
// HTTP DELETE method
router.delete("/contact/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send(`No contact found with given id: ${req.params.id}`);
  Contact.findByIdAndDelete({ _id: req.params.id }, (error, result) => {
    if (error) {
      console.error(`HTTP DELETE error: ${error}`);
      res.json({ msg: `Failed to delete contact with id: ${req.body.id}` });
    } else {
      res.json({ msg: "Contact deleted successfully: ", result });
    }
  });
});

module.exports = router;
