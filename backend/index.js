// env variables
require('dotenv').config();
const api_port = process.env.API_PORT;
const db_user = process.env.DB_USER;
const db_pwd = process.env.DB_PWD;
const db_uri = "mongodb+srv://" + db_user + ":" + db_pwd + "@cluster0.yqvc7dc.mongodb.net/mean-contacts";

// console.log(db_uri); // to check .env variables

// imports
const body_parser = require('body-parser');
const cors = require('cors');
const express = require('express');
const mongoose = require("mongoose");
const path = require("path");

const routes = require('./routes/routes');

// db settings
mongoose.connect(db_uri, (err) => {
    if (!err)
        console.info('MongoDB connection established!');
    else
        console.error('Error in DB connection: ' + JSON.stringify(err, undefined, 2));
});

// start server and use middleware
const app = express();
app.use(cors());
app.use(body_parser.json());

// static files
app.use(express.static(path.join(__dirname, 'public')));

// backend routes
app.use('/api', routes);

// listen on port defined in .env
app.listen(api_port, () => 
    console.info(`Backend server running on port ${api_port}`)
);
