# MEAN CRUD app (contactos)

Temas:

- Creación de una REST API con NodeJS, Express y MongoDB Atlas:
  - Instalación de Express, Mongoose y conexión con MongoDB Atlas
  - Creación de rutas para definir un REST API
  - Declaración de lógica de los métodos CRUD REST
- Creación de una aplicación cliente Angular con:
  - Angular Material
  - Módulos personalizados
  - Formularios con ngModel (Template driven forms)

## Backend

### Pasos iniciales

- crear una cuenta en MongoDB.com y una BD Atlas
- habilitar conexiones remotas desde 0.0.0.0
- (opcional) instalar MongoDB Compass en su máquina local
- almacenar el URL de nuestra conexión
- crear directorio del proyecto: $ mkdir mean-contacts
- $ cd mean-contacts
- crear directorio del backend: $ mkdir backend
- $ cd backend
- inicializar npm: $ npm init -y // default options
- instalar dependencias del backend: $ npm install express mongoose body-parser dotenv nodemon --save
- crear archivo .env: $ touch .env
- editar .env con los valores correspondientes:

```javascript
DB_USER = "usuario_mongodb"
DB_PWD = "contrasegna_mongodb"
API_PORT = 3000
```

- crear script del backend: $ touch index.js
- editar index.js:

```javascript
// env variables
require('dotenv').config();
const api_port = process.env.API_PORT;
const db_user = process.env.DB_USER;
const db_pwd = process.env.DB_PWD;
const db_uri = "mongodb+srv://" + db_user + ":" + db_pwd + "@cluster0.yqvc7dc.mongodb.net/mean-contacts";

// console.log(db_uri); // to check .env variables

// imports
const body_parser = require('body-parser');
const express = require('express');
const mongoose = require("mongoose");
const path = require("path");

// db settings
mongoose.connect(db_uri, (err) => {
    if (!err)
        console.info('MongoDB connection established!');
    else
        console.error('Error in DB connection: ' + JSON.stringify(err, undefined, 2));
});

// start server and use middleware
const app = express();
app.use(body_parser.json());

// server test
app.get('/', (req, res) => {
    res.send('HTTP GET method works');
});

// listen on port defined in .env
app.listen(api_port, () => 
    console.info(`Backend server running on port ${api_port}`)
);
```

- probar el backend: $ node index.js
- editar package.json:

```json
...
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "nodemon index.js"
  },
...
```

- iniciar backend con nodemon: $ npm run start
- navegar a http://localhost:3000 para probar HTTP GET
- si HTTP GET funciona, en index.js borramos el bloque // server test
- crear directorio para rutas: $ mkdir routes
- crear script rutas: $ touch routes/routes.js
- cargar rutas desde index.js:

```javascript
// env variables
require('dotenv').config();
const api_port = process.env.API_PORT;
const db_user = process.env.DB_USER;
const db_pwd = process.env.DB_PWD;
const db_uri = "mongodb+srv://" + db_user + ":" + db_pwd + "@cluster0.yqvc7dc.mongodb.net/mean-contacts";

// console.log(db_uri); // to check .env variables

// imports
const body_parser = require('body-parser');
const express = require('express');
const mongoose = require("mongoose");
const path = require("path");

const routes = require('./routes/routes');

// db settings
mongoose.connect(db_uri, (err) => {
    if (!err)
        console.info('MongoDB connection established!');
    else
        console.error('Error in DB connection: ' + JSON.stringify(err, undefined, 2));
});

// start server and use middleware
const app = express();
app.use(body_parser.json());

// backend routes
app.use('/api', routes);

// listen on port defined in .env
app.listen(api_port, () => 
    console.info(`Backend server running on port ${api_port}`)
);
```

### Configurar rutas API

- editar /backend/routes/routes.js:

```javascript
const express = require('express');
const router = express.Router();

// custom routes

// all contacts
// HTTP GET method
router.get('/contacts', (req, res) => {
    res.send('Retrieving contact list');
});

module.exports = router;
```

- probar nueva ruta en el navegador: http://localhost:3000/api/contacts
- en routes.js crear funciones CRUD:

```javascript
const express = require('express');
const router = express.Router();

// custom routes

// create a contact
// HTTP POST method
router.post('/contact', (req, res) => {
    // POST logic
});

// read all contacts
// HTTP GET method
router.get('/contacts', (req, res) => {
    res.send('Retrieving contact list');
});

// read a contact by id
// HTTP GET method
router.get('/contact/:id', (req, res) => {
    // GET logic
});

// update a contact
// HTTP PUT method
router.put('/contact/:id', (req, res) => {
    // PUT logic
});

// delete a contact
// HTTP DELETE method
router.delete('/contact/:id', (req, res) => {
    // DELETE logic
});

module.exports = router;
```

### Configurar esquema (Mongoose model)

- crear directorio en /backend: $ mkdir models
- crear script en /backend/models: $ touch models/contacts.js:

```javascript
const mongoose = require('mongoose');

// define Schema
const ContactSchema = mongoose.Schema({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    email: { type: String }
})

const Contact = mongoose.model('Contact', ContactSchema);
module.exports = Contact;
```

### Definir lógica CRUD (via controllers)

- importar Mongoose model: agregar en routes.js:

```javascript
const Contact = require('../models/contacts');
```

- vamor a implementar Mongoose queries, mayor información [en la documentación oficial de Mongoose](https://mongoosejs.com/docs/queries.html)
- definir lógica CRUD en routes.js:

```javascript
const express = require("express");
const router = express.Router();
const ObjectId = require("mongoose").Types.ObjectId;

const Contact = require("../models/contacts");

// custom routes

// create a contact
// HTTP POST method
router.post("/contact", (req, res) => {
  let new_contact = new Contact({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
  });
  new_contact.save((error, contact) => {
    if (error) {
      console.error(`HTTP POST error: ${error}`);
      res.json({ msg: "Failed to add contact: ", new_contact });
    } else {
      res.json({ msg: "Contact added successfully", contact });
    }
  });
});

// read all contacts
// HTTP GET method
router.get("/contacts", (req, res) => {
  Contact.find((error, contacts) => {
    if (error) {
      console.error(`HTTP GET error: ${error}`);
      res.json({ msg: "Failed to retrieve contacts" });
    } else {
      res.json(contacts);
    }
  });
});

// read a contact by id
// HTTP GET method
router.get("/contact/:id", (req, res) => {
  let contact_id = req.params.id;
  if (!ObjectId.isValid(contact_id))
    return res
      .status(400)
      .send(`No contact found with given id: ${contact_id}`);
  Contact.findOne({ _id: contact_id }, (error, contact) => {
    if (error) {
      console.error(`HTTP GET error: ${error}`);
      res.json({ msg: "Failed to retrieve contact" });
    } else {
      res.json(contact);
    }
  });
});

// update a contact
// HTTP PUT method
router.put("/contact/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send(`No contact found with given id: ${req.params.id}`);
  const updated_contact = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
  };
  Contact.findByIdAndUpdate(
    req.params.id,
    { $set: updated_contact },
    { new: true }, // option for returning updated document bellow in res.send(document)
    (error, document) => {
      if (!error) {
        res.send(document);
      } else {
        console.error(`Error updating Contact: ${JSON.stringify(error)} `);
      }
    }
  );
});

// delete a contact
// HTTP DELETE method
router.delete("/contact/:id", (req, res) => {
  if (!ObjectId.isValid(req.params.id))
    return res
      .status(400)
      .send(`No contact found with given id: ${req.params.id}`);
  Contact.findByIdAndDelete({ _id: req.params.id }, (error, result) => {
    if (error) {
      console.error(`HTTP DELETE error: ${error}`);
      res.json({ msg: `Failed to delete contact with id: ${req.body.id}` });
    } else {
      res.json({ msg: "Contact deleted successfully: ", result });
    }
  });
});

module.exports = router;
```

- probar operaciones CRUD en la aplicación 'Advanced REST client' => GET http://localhost:3000/api/contacts (debe retornar [ ])
- Hacer POST http://localhost:3000/api/contact => BODY (raw input, JSON)

```json
{
  "first_name": "Homer",
  "last_name": "Simpson",
  "email": "homer.simpson@springfield.us"
}

{
  "first_name": "Jorge",
  "last_name": "Sauceda",
  "email": "jorge@jsauceda.info"
}

{
  "first_name": "ALF",
  "last_name": "alien",
  "email": "alf@zone51.gov"
}
```

- probar de nuevo GET http://localhost:3000/api/contacts (debe retornar [ nuevos usuarios ])
- probar PUT http://localhost:3000/api/contact/_id_Homer BODY (raw input, JSON)

```json
{
  "first_name": "Homer J.",
  "last_name": "Simpson",
  "email": "homer.simpson@springfield.us"
}
```

- probar de nuevo GET http://localhost:3000/api/contacts (debe retornar [ usuarios con 'Homer J. Simpson' ])
- probar DELETE http://localhost:3000/api/contact/_id_ALF
- probar de nuevo GET http://localhost:3000/api/contacts (debe retornar [ usuarios sin ALF ])

### Configurar static files (opcional - no implementado)

En caso de desear presentar una página web al acceder al backend:

- crear directorio en /backend: $ mkdir public
- crear html: $ touch public/index.html
- configurar static files en index.js:

```javascript
// static files
app.use(express.static(path.join(__dirname, 'public')));

// backend routes
...
```

### Habilitar CORS

- ¿Qué es CORS? Ver respuestas en [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS) y [Auth0](https://auth0.com/blog/cors-tutorial-a-guide-to-cross-origin-resource-sharing/)
- $ npm install cors
- editar index.js:

```javascript
...
// imports
const body_parser = require('body-parser');
const cors = require('cors');
...

// start server and use middleware
const app = express();
app.use(cors());
app.use(body_parser.json());
...
```

## Frontend

- crear directorio frontend en /: $ ng new frontend // sin routing, con CSS
- entrar en el directorio: $ cd frontend
- instalar Material: $ ng add @angular/material // tema purple-green, typography YES, incluir y activar animations
- el comando anterior modifica styles.css, index.html y package.json
- mayor información sobre Angular Material en [la documentación oficial](https://material.angular.io/guide/getting-started), [este 36 min video](https://www.youtube.com/watch?v=wPT3K3w6JtU), o [este 2 h video](https://www.youtube.com/watch?v=DaE_RpWRlJI)
- en /src/app crear módulo para usar material: $ touch material.module.ts:

```javascript
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  exports: [
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatListModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class MaterialModule {}
```

- editar app.module.ts:

```javascript
...
import { MaterialModule } from "./material.module";
...
@NgModule({
	...
    imports: [
    	...
        MaterialModule,
    ]
})
```

- crear componentes 'header', 'contacts', 'about' y 'tabs':
- $ ng generate component components/header --module app
- $ ng generate component components/contacts --module app
- $ ng generate component components/about --module app
- $ ng generate component components/tabs --module app
- editar header.component.ts:

```javascript
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent {
  @Input() title!: string;
}
```

- editar header.component.html:

```xml
<mat-toolbar color="primary">
  <span>{{ title }}</span>
</mat-toolbar>
```

- editar about.component.html:

```xml
<mat-card backgroundColor="primary" [ngStyle]="{ 'margin-top': '30px' }">
  <mat-card-header>
    <mat-card-title>
      <h2>Contact Manager</h2>
    </mat-card-title>
  </mat-card-header>
  <mat-card-content>
    <p>Esta aplicación ha sido creada usando:</p>
    <mat-list>
      <mat-list-item>MongoDB</mat-list-item>
      <mat-list-item>Express</mat-list-item>
      <mat-list-item>Angular</mat-list-item>
      <mat-list-item>Node</mat-list-item>
    </mat-list>
    <mat-divider></mat-divider>
  </mat-card-content>
  <mat-card-actions [ngStyle]="{ 'margin-left': '10px' }">
    <p>Coded by &nbsp;</p>
    <a mat-flat-button color="accent" target="https://gitlab.com/j_sauceda">
      Jorge Sauceda &copy; 2022
    </a>
  </mat-card-actions>
</mat-card>
```

- editar about.component.css:

```css
h2 {
  color: white;
}

p {
  color: white;
}
```

- editar tabs.component.html:

```xml
<mat-tab-group backgroundColor="primary" color="accent">
  <mat-tab label="Main">
    <app-contacts></app-contacts>
  </mat-tab>
  <mat-tab label="About">
    <app-about></app-about>
  </mat-tab>
</mat-tab-group>
```

- editar app.components.ts:

```JS
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Contact Manager';
}
```

- editar app.component.html:

```xml
<app-header [title]="title"></app-header>
<app-tabs></app-tabs>
```

- $ ng serve

### Consumir datos del backend

- crear directorio compartido: $ mkdir shared
- crear interfaz: $ touch shared/contact.ts:

```JS
export interface Contact {
  _id?: string;
  first_name: string;
  last_name: string;
  email: string;
}
```

- generar servicio 'contact': $ ng g service services/contact
- editar contact.service.ts:

```xml
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Contact } from '../shared/contact';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  private api_url = 'http://localhost:3000/api';

  constructor(private http: HttpClient) {}

  get_contacts(): Observable<Contact[]> {
    const url = `${this.api_url}/contacts`;
    return this.http.get<Contact[]>(url);
  }

  get_contact_by_id(id: string): Observable<Contact> {
    const url = `${this.api_url}/contact/${id}`;
    return this.http.get<Contact>(url);
  }

  delete_contact(contact: Contact): Observable<Contact> {
    const url = `${this.api_url}/contact/${contact._id}`;
    return this.http.delete<Contact>(url);
  }

  update_contact(contact: Contact): Observable<Contact> {
    const url = `${this.api_url}/contact/${contact._id}`;
    return this.http.put<Contact>(url, contact, httpOptions);
  }

  add_contact(contact: Contact): Observable<Contact> {
    const url = `${this.api_url}/contact`;
    return this.http.post<Contact>(url, contact, httpOptions);
  }
}
```

- editar app.module.ts:

```javascript
...
import { HttpClientModule } from '@angular/common/http';
...
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
  ], ...
```

- editar contacts.component.ts:

```javascript
import { Component, OnInit } from '@angular/core';

import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/shared/contact';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  providers: [ContactService],
})
export class ContactsComponent implements OnInit {
  contacts!: Contact[];

  displayedColumns: string[] = ['first_name', 'last_name', 'email', '_id'];
  editableContact: Contact | null = null;
  editionMode = false;

  constructor(private contactService: ContactService) {}

  ngOnInit(): void {
    this.get_contacts();
  }

  get_contacts() {
    this.contactService.get_contacts().subscribe((all_contacts) => {
      this.contacts = all_contacts;
    });
  }
  
  search_contact(id: string) {
    this.contactService.get_contact_by_id(id).subscribe((contact) => {
      this.editableContact = contact;
    });
  }

  delete_contact(contact: Contact) {
    this.contactService.delete_contact(contact).subscribe(() => {
      this.contacts = this.contacts.filter((c) => c._id !== contact._id);
    });
  }

  update_contact(contact: Contact) {
    this.contactService.update_contact(contact).subscribe(() => {
      this.get_contacts();
    });
  }

  add_contact(first_name: string, last_name: string, email: string) {
    if (!first_name || !last_name || !email) return;
    let new_contact: Contact = { first_name, last_name, email };
    this.contactService.add_contact(new_contact).subscribe((contact) => {
      this.contacts.push(contact);
    });
  }
}
```

- editar contacts.component.html:

```xml
<div [ngStyle]="{ 'margin-top': '30px' }">
  <!-- New contact form -->
  <h2 [ngStyle]="{ 'margin-left': '20%' }" *ngIf="!editionMode">Add Contact</h2>

  <form
    class="form-container"
    [ngStyle]="{ 'margin-left': '20%' }"
    (submit)="
      add_contact(firstName.value, lastName.value, email.value);
      firstName.value = '';
      lastName.value = '';
      email.value = ''
    "
    *ngIf="!editionMode"
  >
    <mat-form-field class="wide-input">
      <mat-label>First Name</mat-label>
      <input
        #firstName
        id="first_name"
        matInput
        name="first_name"
        placeholder="First Name"
        type="text"
      />
    </mat-form-field>

    <mat-form-field class="wide-input">
      <mat-label>Last Name</mat-label>
      <input
        #lastName
        matInput
        id="last_name"
        name="last_name"
        placeholder="Last Name"
        type="text"
      />
    </mat-form-field>

    <mat-form-field class="wide-input">
      <mat-label>Email</mat-label>
      <input
        #email
        matInput
        id="email"
        name="email"
        placeholder="Email"
        type="email"
      />
    </mat-form-field>

    <button
      matInput
      mat-flat-button
      [ngStyle]="{
        'margin-top': '-18px',
        'margin-left': '10px',
        padding: '28px'
      }"
      color="accent"
      type="submit"
    >
      Add
    </button>
  </form>

  <!-- Edit Contact Form -->
  <h2 *ngIf="editionMode" [ngStyle]="{ 'margin-left': '20%' }">Edit Contact</h2>
  <div
    *ngIf="
      editableContact.first_name &&
      editableContact.last_name &&
      editableContact.email
    "
    [ngStyle]="{ 'margin-left': '20%' }"
  >
    {{ editableContact | json }}
    <div class="button-row">
      <button
        mat-flat-button
        color="accent"
        [ngStyle]="{
          'margin-top': '20px',
          'margin-left': '10px',
          padding: '28px'
        }"
        (click)="
          update_contact(editableContact);
          editableContact.first_name = '';
          editableContact.last_name = '';
          editableContact.email = '';
          editableContact._id = undefined;
          editionMode = !editionMode
        "
      >
        Save
      </button>
      &nbsp;
      <button
        mat-flat-button
        color="warn"
        [ngStyle]="{
          'margin-top': '20px',
          'margin-left': '10px',
          padding: '28px'
        }"
        (click)="
          editableContact.first_name = '';
          editableContact.last_name = '';
          editableContact.email = '';
          editableContact._id = undefined;
          editionMode = !editionMode
        "
      >
        Cancel
      </button>
    </div>
  </div>

  <!-- Contacts table -->
  <table mat-table [dataSource]="contacts" *ngIf="!editionMode">
    <!-- FirstName Column -->
    <ng-container matColumnDef="first_name">
      <mat-header-cell *matHeaderCellDef>First Name</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.first_name | titlecase }}
      </mat-cell>
    </ng-container>

    <!-- LastName Column -->
    <ng-container matColumnDef="last_name">
      <mat-header-cell *matHeaderCellDef>Last Name</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.last_name | titlecase }}
      </mat-cell>
    </ng-container>

    <!-- Email Column -->
    <ng-container matColumnDef="email">
      <mat-header-cell *matHeaderCellDef>Email</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.email | lowercase }}
      </mat-cell>
    </ng-container>

    <!-- Buttons Column -->
    <ng-container matColumnDef="_id">
      <mat-header-cell *matHeaderCellDef>Action Buttons</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        <button
          mat-flat-button
          color="accent"
          (click)="search_contact(contact._id); editionMode = !editionMode"
        >
          Edit
        </button>
        &nbsp;
        <button mat-flat-button color="warn" (click)="delete_contact(contact)">
          Delete
        </button>
      </mat-cell>
    </ng-container>

    <!-- Table settings -->
    <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
    <mat-row *matRowDef="let row; columns: displayedColumns"></mat-row>
  </table>
</div>
```

- editar contacts.component.css:

```css
.button-row {
  display: table-cell;
  max-width: 600px;
}

.form-container mat-form-field + mat-form-field {
  margin-left: 8px;
}

.wide-input {
  width: 20%;
}
```

### Formularios con ngModel (Template driven forms)

- editar app.module.ts:

```javascript
...
import { FormsModule } from '@angular/forms';
...
@NgModule({
  ...
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
  ]
})
export class AppModule {}
```

- editar contacts.component.ts (importar NgForm & ViewChild, crear addForm, editForm, eliminar editableContact y modificar métodos search_contact(), update_contact() y add_contact() ):

```javascript
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ContactService } from 'src/app/services/contact.service';
import { Contact } from 'src/app/shared/contact';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
  providers: [ContactService],
})
export class ContactsComponent implements OnInit {
  contacts!: Contact[];

  displayedColumns: string[] = ['first_name', 'last_name', 'email', '_id'];
  edit_id: string | undefined = undefined;
  @ViewChild('newContactForm') addForm!: NgForm;
  @ViewChild('editContactForm') editForm!: NgForm;
  editionMode = false;

  constructor(private contactsService: ContactService) {}

  ngOnInit(): void {
    this.get_contacts();
  }

  get_contacts() {
    this.contactsService.get_contacts().subscribe((all_contacts) => {
      this.contacts = all_contacts;
    });
  }

  search_contact(id: string) {
    this.edit_id = id;
    this.contactsService.get_contact_by_id(id).subscribe((contact) => {
      this.editForm.setValue({
        first_name: contact.first_name,
        last_name: contact.last_name,
        email: contact.email,
        _id: contact._id,
      });
    });
  }

  delete_contact(contact: Contact) {
    this.contactsService.delete_contact(contact).subscribe(() => {
      this.contacts = this.contacts.filter((c) => c._id !== contact._id);
    });
  }

  update_contact() {
    this.contactsService
      .update_contact({ _id: this.edit_id, ...this.editForm.value })
      .subscribe(() => {
        this.get_contacts();
      });
  }

  add_contact() {
    this.contactsService.add_contact(this.addForm.value).subscribe(() => {
      this.get_contacts();
    });
  }
}
```

- editar contacts.component.html:

```xml
<div [ngStyle]="{ 'margin-top': '30px' }">
  <!-- New contact form -->
  <div *ngIf="!editionMode">
    <h2 [ngStyle]="{ 'margin-left': '20%' }">Add Contact</h2>

    <form
      class="form-container"
      id="addContactForm"
      [ngStyle]="{ 'margin-left': '20%' }"
      (ngSubmit)="add_contact(); newContactForm.resetForm()"
      #newContactForm="ngForm"
    >
      <!-- FirstName input -->
      <mat-form-field class="wide-input">
        <mat-label>First Name</mat-label>
        <input
          matInput
          ngModel
          #first_name="ngModel"
          name="first_name"
          id="first_name"
          placeholder="First Name"
          required
        />
        <mat-error *ngIf="!first_name.valid && first_name.touched">
          First Name is required
        </mat-error>
      </mat-form-field>

      <!-- LastName input -->
      <mat-form-field class="wide-input">
        <mat-label>Last Name</mat-label>
        <input
          matInput
          ngModel
          #last_name="ngModel"
          name="last_name"
          id="last_name"
          placeholder="Last Name"
          required
        />
        <mat-error *ngIf="!last_name.valid && last_name.touched">
          Last Name is required
        </mat-error>
      </mat-form-field>

      <!-- Email input -->
      <mat-form-field class="wide-input">
        <mat-label>Email</mat-label>
        <input
          matInput
          ngModel
          #email="ngModel"
          name="email"
          id="email"
          placeholder="Contact email"
          required
          email
        />
        <mat-error *ngIf="!email.valid && email.touched">
          Contact email is required
        </mat-error>
      </mat-form-field>

      <!-- Submit button -->
      <button
        matInput
        mat-flat-button
        [disabled]="!newContactForm.form.valid"
        [ngStyle]="{
          'margin-top': '-18px',
          'margin-left': '10px',
          padding: '28px'
        }"
        color="accent"
        type="submit"
      >
        Add
      </button>
    </form>
  </div>

  <!-- Edit contact form -->
  <h2 *ngIf="editionMode" [ngStyle]="{ 'margin-left': '20%' }">Edit Contact</h2>
  <form
    class="form-container"
    id="editContactForm"
    *ngIf="editionMode"
    [ngStyle]="{ 'margin-left': '20%' }"
    (ngSubmit)="update_contact(); editionMode = !editionMode"
    #editContactForm="ngForm"
  >
    <!-- FirstName input -->
    <mat-form-field class="wide-input">
      <mat-label>First Name</mat-label>
      <input
        matInput
        ngModel
        name="first_name"
        #first_name="ngModel"
        placeholder="First Name"
        required
      />
      <mat-error *ngIf="!first_name.valid && first_name.touched">
        First Name is required
      </mat-error>
    </mat-form-field>

    <!-- LastName input -->
    <mat-form-field class="wide-input">
      <mat-label>Last Name</mat-label>
      <input
        matInput
        ngModel
        name="last_name"
        #last_name="ngModel"
        placeholder="Last Name"
        required
      />
      <mat-error *ngIf="!last_name.valid && last_name.touched">
        Last Name is required
      </mat-error>
    </mat-form-field>

    <!-- Email input -->
    <mat-form-field class="wide-input">
      <mat-label>Email</mat-label>
      <input
        matInput
        ngModel
        name="email"
        #email="ngModel"
        placeholder="Email"
        required
      />
      <mat-error *ngIf="!email.valid && email.touched">
        Email is required
      </mat-error>
    </mat-form-field>

    <!-- Action buttons -->
    <div class="button-row">
      <button
        mat-flat-button
        color="accent"
        [disabled]="!editContactForm.form.valid"
        [ngStyle]="{
          'margin-top': '20px',
          'margin-left': '10px',
          padding: '28px'
        }"
        type="submit"
      >
        Save
      </button>
      &nbsp;
      <button
        mat-flat-button
        color="warn"
        [ngStyle]="{
          'margin-top': '20px',
          'margin-left': '10px',
          padding: '28px'
        }"
        (click)="editionMode = !editionMode"
      >
        Cancel
      </button>
    </div>
  </form>

  <!-- Contacts table -->
  <table
    mat-table
    [dataSource]="contacts"
    [ngStyle]="{ 'margin-top': '30px' }"
    *ngIf="!editionMode"
  >
    <!-- FirstName column -->
    <ng-container matColumnDef="first_name">
      <mat-header-cell *matHeaderCellDef>First Name</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.first_name | titlecase }}
      </mat-cell>
    </ng-container>

    <!-- LastName column -->
    <ng-container matColumnDef="last_name">
      <mat-header-cell *matHeaderCellDef>Last Name</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.last_name | titlecase }}
      </mat-cell>
    </ng-container>

    <!-- Email column -->
    <ng-container matColumnDef="email">
      <mat-header-cell *matHeaderCellDef>Email</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        {{ contact.email | lowercase }}
      </mat-cell>
    </ng-container>

    <!-- Buttons column -->
    <ng-container matColumnDef="_id">
      <mat-header-cell *matHeaderCellDef>Action Buttons</mat-header-cell>
      <mat-cell *matCellDef="let contact">
        <button
          mat-flat-button
          color="accent"
          (click)="search_contact(contact._id); editionMode = !editionMode"
        >
          Edit
        </button>
        &nbsp;
        <button mat-flat-button color="warn" (click)="delete_contact(contact)">
          Delete
        </button>
      </mat-cell>
    </ng-container>

    <!-- Table settings -->
    <mat-header-row *matHeaderRowDef="displayedColumns"></mat-header-row>
    <mat-row *matRowDef="let row; columns: displayedColumns"></mat-row>
  </table>
</div>
```
